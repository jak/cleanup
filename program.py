#!/usr/bin/python
#
# program.py - Pseudo-boolean optimization programs
#
# Copyright (C) 2012 Julian Andres Klode <jak@debian.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""Pseudo-boolean optimization programs.

This module defines a class for representing pseudo-boolean optimization
problems. It can render in PBO and LP formats.
"""

import sys
from collections import OrderedDict


class Constraint(object):
    """A single linear constraint"""

    __slots__ = ['prog', 'is_lp', 'op', 'lhs', 'rhs', 'count', 'comment']

    def __init__(self, prog, op, rhs, comment):
        if op not in (">", ">=", "=", "<=", "<"):
            raise ValueError("Invalid operator: %s" % op)
        if not isinstance(rhs, int):
            raise TypeError("Right-hand-side must be an integer")

        # Metadata
        self.prog = prog
        self.is_lp = prog.is_lp
        self.count = len(prog.constraints) + 1
        self.comment = comment

        # The constraint itself
        self.lhs = []
        self.op = op
        self.rhs = rhs

    def add(self, value, var):
        """Add value * var to the lhs"""
        assert isinstance(var, int) or isinstance(var, long)
        self.lhs.append((value, self.prog.to_clasp(var)))
        return self

    def finalize(self, buf):
        """Finalize the given constraint, write it to the buffer.

        :buf: A line buffer, where strings are added with 'append'
        """
        if self.comment:
            buf.append("%s* %s%s" % ("/" if self.is_lp else "",
                                     self.comment,
                                     "/" if self.is_lp else ""))
        line = ""
        if self.is_lp:
            line += "con_%i: " % (self.count)
        for value, var in self.lhs:
            line += "%i x%i " % (value, var)

        buf.append(line + "%s %i;" % (self.op, self.rhs))


class Program(object):
    """A single PBO instance"""

    __slots__ = ['vars', 'constraints', 'is_lp', 'last_result', '__to_clasp', '__from_clasp']

    MAX_WEIGHT = 2**31
    MIN_WEIGHT = -2**30

    def __init__(self, is_lp=False):
        """
        :lp: Whether to use a linear program format
        """
        self.vars = OrderedDict()
        self.constraints = []
        self.is_lp = is_lp
        self.last_result = None
        self.__to_clasp = {}
        self.__from_clasp = {}

    def to_clasp(self, var):

        try:
            return self.__to_clasp[var]
        except:
            id = len(self.__to_clasp) + 1
            self.__to_clasp[var] = id
            self.__from_clasp[id] = var
            return id

    def from_clasp(self, id):
        try:
            return self.__from_clasp[id]
        except:
            var = len(self.__from_clasp) + 1
            self.__to_clasp[var] = id
            self.__from_clasp[id] = var
            return id

    def set_weight(self, var, weight):
        """Set the weight of a variable"""
        if weight > self.MAX_WEIGHT or weight < self.MIN_WEIGHT:
            raise ValueError("Weight exceeds bounds: %i of %s" % (weight, var))

        self.vars[self.to_clasp(var)] = weight

    def start(self, op, rhs, comment=None):
        """Start a new constraint"""
        constraint = Constraint(self, op, int(rhs), comment)
        self.constraints.append(constraint)
        return constraint

    def finalize(self):
        """Render this program as a string."""
        buf = []
        if self.is_lp:
            buf.append("/*")
        buf.append("* #variable= %i #constraint= %i" % (len(self.vars),
                                                        len(self.constraints)))
        buf.append("*")
        buf.append("* Created by Program.py")
        buf.append("*")
        if self.is_lp:
            buf.append("*/")

        line = "min: "
        for var, value in sorted(self.vars.items()):
            line += "%s%i x%i " % ("+" if value > 0 else "", value, var)
        buf.append(line + ";")
        for constraint in self.constraints:
            constraint.finalize(buf)

        if self.is_lp:
            for var in self.vars.iterkeys():
                buf.append("bin x%i;" % var)

        return "\n".join(buf)

    def new_criterion(self):
        """Start a new criterion.

        This will save the current objective function as a hard constraint,
        allowing you to add a new criterion for multi-criteria solving.
        """
        if self.last_result is None:
            raise SystemError("Not run yet")

        constraint = self.start(">=", -self.last_result, "previous objective")
        for var in self.vars:
            constraint.add(-self.vars[var], self.from_clasp(var))

        self.vars.clear()

    def run(self):
        print("Running clasp on ", len(self.constraints), "constraints and", len(self.vars), "variables ...", file=sys.stderr)
        import subprocess

        if "--minisat" in sys.argv:
            proc = subprocess.Popen(["minisat+",  "-ansi", "-cs", "/dev/stdin"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        else:
            proc = subprocess.Popen(["clasp", "--opt-heuristic=3",  "/dev/stdin"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        out, err = proc.communicate(self.finalize().encode("utf-8"))
        result = set()

        if proc.returncode not in (0, 10, 30):  # clasp exit codes are broken?
            with open("program", "w") as outf:
                outf.write(self.finalize())
            raise OSError("Exited with return code %s and message: %s" % (proc.returncode, out))

        for line in out.decode("utf-8").splitlines():
            if line.strip().startswith("s"):
                if line.split()[1] == "UNSATISFIABLE":
                    raise SystemError("UNSATISFIABLE")
            elif line.strip().startswith("c Found solution:"):
                self.last_result = int(line.split(":")[1])
            elif line.strip().startswith("o"):
                self.last_result = int(line.split()[1])
            elif line.strip().startswith("v"):
                for v in line.split()[1:]:
                    vals = v.strip().split("x")
                    if len(vals) == 2:
                        if vals[0] == '-' or vals[1][0] == '-':
                            pass
                        elif vals[0] == '':
                            result.add(self.from_clasp(int(vals[1])))
                        else:
                            raise ValueError("Invalid value: " + v + str(vals))
                    else:
                        raise ValueError("Invalid value: " + v)
            elif "c Time" in line:
                 print("Ignore", line)
        return result



if __name__ == '__main__':
    p = Program(is_lp="--lp" in sys.argv)
    p.set_weight(0, 5)
    p.start(">=", 0).add(5, 0)
    print(p.finalize())
    print(p.run())
