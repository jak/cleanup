#!/usr/bin/python3
#
# program_builder.py - An APT -> PBO problem builder
#
# Copyright (C) 2012 Julian Andres Klode <jak@debian.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""Convert an APT cache to a program."""

import argparse
import sys

import apt_pkg

from program import Program


def pkg_name(v):
    """Get a name of the version"""
    return "%s=%s#%i" % (v.parent_pkg.get_fullname(), v.ver_str, v.id)


class ProgramBuilder(object):

    def __init__(self, is_lp=False):
        self.program = Program(is_lp=is_lp)
        self.cache = apt_pkg.Cache(progress=None)
        self.depcache = apt_pkg.DepCache(self.cache)
        self.criterion = None

        self.to_install = set()
        self.to_remove = set()
        self.softdeps = {"Recommends"}
        self.hard_softdeps = True

    def init_package_map(self):
        """Initialize the package mapping."""
        if True:
            self.packages = {p.id: p for p in self.cache.packages if p.current_ver is not None or p.id in self.to_install or p.id in self.to_remove}
            self.visited = set()
            packages_len = len(self.packages)
            while True:
                for p in list(self.packages.values()):
                    if p.id in self.visited:
                        continue
                    self.visited.add(p.id)
                    for v in p.version_list:
                        for ors in v.depends_list.values():
                            for or_ in ors:
                                for dep in or_:
                                    for v in dep.all_targets():
                                        self.packages[v.parent_pkg.id] = v.parent_pkg 


                if packages_len == len(self.packages):
                    break

                packages_len = len(self.packages)
        else:
            self.packages = {p.id: p for p in self.cache.packages if p.has_versions}
            
        self.packages = sorted([(p, p.version_list) for p in self.packages.values()], key=lambda t: t[0].id)
        self.versions = {}
        
        for p, versions in self.packages:
            for v in versions:
                self.versions[v.id] = v


    def __is_hard_install(self, p):
        return ((p.essential and p.architecture == apt_pkg.config["APT::Architecture"]) or
                (p.important and p.current_ver is not None) or
                (p.id in self.to_install))

    def mark_install(self, pkg):
        """Mark a package for installation."""
        self.to_install.add(self.cache[pkg].id)

    def mark_delete(self, pkg):
        """Mark a package for removal."""
        self.to_remove.add(self.cache[pkg].id)

    def set_criterion(self, criterion):
        if criterion == "new":    # Minimize new
            for package, versions in self.packages:
                [self.program.set_weight(v.id, int(package.current_ver is None)) for v in versions]
        elif criterion == "changed":
            for package, versions in self.packages:
                [self.program.set_weight(v.id, 0) for v in versions]
                if package.current_ver is not None:
                    self.program.set_weight(package.current_ver.id, -1)
        elif criterion == "removed":
            for package, versions in self.packages:
                if package.current_ver is None:
                    [self.program.set_weight(v.id, 0) for v in versions]
                else:
                    [self.program.set_weight(v.id, -1) for v in versions]
        elif criterion == "removeauto":
            for package, versions in self.packages:
                if (package.current_ver is not None
                    and self.depcache.is_auto_installed(package)):
                    [self.program.set_weight(v.id, 1) for v in versions]
                else:
                    [self.program.set_weight(v.id, 0) for v in versions]
        elif criterion == "keepmanual":
            for package, versions in self.packages:
                if (package.current_ver is not None
                    and not self.depcache.is_auto_installed(package)):
                    [self.program.set_weight(v.id, -1) for v in versions]
                else:
                    [self.program.set_weight(v.id, 0) for v in versions]
        elif criterion == "notuptodate":
            for package, versions in self.packages:
                [self.program.set_weight(v.id, 0) for v in versions]
                if package.current_ver is not None:
                    cand = self.depcache.get_candidate_ver(package)
                    if cand is not None:
                        self.program.set_weight(cand.id, -1)
        else:
            raise ValueError("Unknown criterion: %s" % criterion)

        self.criterion = criterion


    def _is_satisfied(self, group):
        """Check whether a dependency group is currently satisfied."""
        for dep in group:
            for tgt in dep.all_targets():
                tgt_cv = tgt.parent_pkg.current_ver
                if tgt_cv is not None and tgt.id == tgt_cv.id:
                    return True
        return False

    def _handle_positive(self, package, version, groups, soft):
        """Handle a positive dependency statement.

        Each positive dependency group is represented by:

            - pkg + targets >= 0

        If a group can be satisfied by a single target only, it is represented
        as:

            - n * pkg + targets of those groups >= 0

        Causing all groups to be installed if pkg is installed.
        """

        simple = [] # Optimization

        for grp in groups:
            if not soft and (len(grp) == 1 and len(grp[0].all_targets()) == 1):
                simple.append(grp[0].all_targets()[0])
                continue

            if soft and (package.current_ver == version) and not self._is_satisfied(grp):
                continue

            con = self.program.start(">=", 0)
            con.add(-1, version.id)

            for dep in grp:
                for tgt in dep.all_targets():
                    con.add(1, tgt.id)

        if len(simple) > 0:
            con = self.program.start(">=", 0)
            con.add(-len(simple), version.id)
            for s in simple:
                con.add(1, s.id)

    def _handle_negative(self, version, groups):
        """Handle a negative dependency of a versions.

        Negative dependencies are simple to handle and have the form:

            n * pkg + sum(conflicts) <= n  <=>  -n * pkg - sum(conflicts) >= -n
        """

        # Determine all conflict targets
        cons = []
        for grp in groups:
            for dep in grp:
                for tgt in dep.all_targets():
                    cons.append(tgt)

        if cons:
            con = self.program.start(">=", -len(cons), "Negative deps of %s" % (pkg_name(version)))
            for con_ in cons:
                con.add(-1, con_.id)
            con.add(-len(cons), version.id)

    def add_constraints(self):
        """Add the constraints to the program."""

        print("Adding constraints...")

        prog = self.program
        for p, versions in self.packages:
            if p.id in self.to_remove:
                con = prog.start("=", 0)
                for v in versions:
                    con.add(1, v.id)
            elif self.__is_hard_install(p):
                con = prog.start("=", 1)
                for v in versions:
                    con.add(1, v.id)
            if len(p.version_list) > 1:
                # Do not have more than one version of that package installed in parallel
                con = prog.start(">=", -1)
                for v in versions:
                    con.add(-1, v.id)

        for p, versions in self.packages:
            for v in versions:
                g = 0
                # Positive Dependencies:
                for (t, gs) in v.depends_list.items():
                    if t in ("Depends", "Pre-Depends", "PreDepends"):
                        self._handle_positive(p, v, gs, soft=False)
                    elif t in self.softdeps and self.hard_softdeps:
                        self._handle_positive(p, v, gs, soft=True)
                    elif t in ("Conflicts", "Breaks"):
                        self._handle_negative(v, gs)

    def run(self, print_result=False):
        if self.criterion is not None:
            print("Running optimization", self.criterion)
        current = set()

        action = apt_pkg.ActionGroup(self.depcache)

        for p, _ in self.packages:
            c = p.current_ver
            if c is not None:
                current.add(c.id)

        result = self.program.run()

        if not print_result:
            return result

        for c in current:
            v = self.versions[c]
            if c not in result:
                self.depcache.mark_delete(v.parent_pkg)

        for c in result:
            v = self.versions[c]
            if c not in current:
                self.depcache.set_candidate_ver(v.parent_pkg, v)
                self.depcache.mark_install(v.parent_pkg, False)

        print("Results:")
        for p, _ in sorted(self.packages, key=lambda p: p[0].name):
            if p.current_ver is not None and self.depcache.marked_downgrade(p):
                print("    Downgrade", p.get_fullname(pretty=True), p.current_ver.ver_str, self.depcache.get_candidate_ver(p).ver_str)
            if p.current_ver is not None and self.depcache.marked_upgrade(p):
                print("    Upgrade", p.get_fullname(pretty=True), p.current_ver.ver_str, self.depcache.get_candidate_ver(p).ver_str)
            elif self.depcache.marked_install(p):
                print("    Install", p.get_fullname(pretty=True), self.depcache.get_candidate_ver(p).ver_str)
            elif self.depcache.marked_delete(p):
                print("    Remove", p.get_fullname(pretty=True))

        return result


def main():
    """Main program code"""

    apt_pkg.init()

    parser = argparse.ArgumentParser(description='Cleanup tool')
    parser.add_argument("-c", dest="criteria", action="append",
                        help="Select a criterion",
                        choices=["new", "changed", "removed", "notuptodate", "keepmanual", "removeauto"])
    parser.add_argument("-u", dest="criteria", action="store_const",
                        const=["notuptodate", "new", "changed"],
                        help="Use the update criteria")
    parser.add_argument("-i", dest="criteria", action="store_const",
                        const=["changed", "new"],
                        help="Use the install criteria")
    parser.add_argument("-R", dest="recommends", action="store_false",
                        help="Do not install/keep recommends")
    parser.add_argument("--minisat", dest="minisat", action="store_true",
                        help="Use the minisat+ solver")
    parser.add_argument("packages", nargs="*")


    options = parser.parse_args()

    print(options)

    if not options.criteria:
        # Ignore the sources.list and source parts
        apt_pkg.config["Dir::Etc::SourceList"] = "/dev/null"
        apt_pkg.config["Dir::Etc::SourceParts"] = "/dev/null"
        del apt_pkg.config["APT::Default-Release"]
        options.criteria = ["keepmanual", "removeauto"]

    # Create the program
    pb = ProgramBuilder()

    if not options.recommends:
        pb.softdeps = set()
        pb.hard_softdeps = False

    for i in options.packages:
        try:
            pb.mark_install(i)
        except KeyError:
            if i[-1] == '-':
                pb.mark_delete(i[:-1])
            else:
                raise

    pb.init_package_map()
    pb.add_constraints()

    for criterion in options.criteria:
        if criterion != options.criteria[0]:
            pb.program.new_criterion()

        pb.set_criterion(criterion)
        pb.run(print_result=(criterion == options.criteria[-1]))


if __name__ == '__main__':
    main()
